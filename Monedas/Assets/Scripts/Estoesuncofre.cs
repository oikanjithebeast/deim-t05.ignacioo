using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Estoesuncofre: MonoBehaviour
{
    public int coinValue = 1;
    public Animator cofreabre;
  
    private void OnTriggerStay2D(Collider2D collision)
    {
        if (collision.GetComponent<Collider2D>().CompareTag("Player"))
        {
            Debug.Log("Hi");
            if (Input.GetKey(KeyCode.W))
            {
                Debug.Log("Hol");
                cofreabre.SetBool("open", true);
                contadordemonedas.SumaMoneda(coinValue);
            }
        }
    }
}
