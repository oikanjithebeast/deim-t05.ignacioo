using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movimiento_personaje : MonoBehaviour
{

    public Animator personajeAnimator;

    public SpriteRenderer personajeSprite;

    public Rigidbody2D rigidbody2d;

    public RaycastHit2D rchit2d;

    public float vel;

    public bool Suelo;

    public float velSalto;

    public float distance;

    private void Start()
    {
        rigidbody2d = GetComponent<Rigidbody2D>();


    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.A))
        {

            personajeAnimator.SetBool("Run", true);
            transform.position += Vector3.left * vel;
            personajeSprite.flipX = true;

        }

        else if (Input.GetKey(KeyCode.D)){

            
            personajeAnimator.SetBool("Run", true);
            transform.position += Vector3.right * vel;

            personajeSprite.flipX = false;
            

        }

        else
        {

            personajeAnimator.SetBool("Run", false);

        }

        if (Physics2D.Raycast(transform.position, Vector3.down, 0.1f))
        {
            Suelo = true;
        }   
        else Suelo = false;

        if (Input.GetKeyDown(KeyCode.Space) && Suelo)
        {
            personajeAnimator.SetBool("Jumpup", true);
            //transform.position += Vector3.up * velSalto;
            rigidbody2d.velocity = Vector3.up * velSalto;
        }
        
        if(rigidbody2d.velocity.y < 0)
        {
            personajeAnimator.SetBool("Jumpup", false);
        }



        //rchit2d = Physics2D.Raycast(gameObject.transform.position, Vector2.down, distance);

        //if(rchit2d.collider != null)
        //{
            //boolean = false;
        //}

        //if (rchit2d.transform.tag == "Terreno")
        //{
            //boolean = true;
        //}


    }


    


}
