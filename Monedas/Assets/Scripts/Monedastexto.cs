using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class Monedastexto : MonoBehaviour
{
    public static Monedastexto instance;
    public TextMeshProUGUI text;
    private int coinScore;
    // Start is called before the first frame update
    void Start()
    {
        if (instance == null)
            instance = this;
    }

    // Update is called once per frame
    public void ChangeCoinScore(int coinValue)
    {
        coinScore += coinValue;
        text.text = "x" + coinScore.ToString();
    }
    void Update()
    {
        
    }
}
